class Order < ApplicationRecord
  belongs_to :cashier
  has_many :orderlines, inverse_of: :order
  accepts_nested_attributes_for :orderlines, reject_if: :all_blank, allow_destroy: true
end
