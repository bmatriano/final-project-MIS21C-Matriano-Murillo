class Product < ApplicationRecord
	def deactivate
		update_attribute :status,false
	end
	def activate
		update_attribute :status,true
	end
	has_many :orderlines
end
