class Cashier < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthables
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
	  devise :database_authenticatable, :registerable,
			 :recoverable, :rememberable, :trackable, :validatable
	  belongs_to :admin
	  has_many :orders
	  validates :name, presence: true, uniqueness: true
  	def deactivate
		update_attribute :active,false
	end
	def activate
		update_attribute :active,true
	end
end
