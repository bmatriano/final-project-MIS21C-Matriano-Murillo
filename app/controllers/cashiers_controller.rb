class CashiersController < ApplicationController
	before_action :authenticate_admin!
 	def new
	end
	def show
		@cashier = Cashier.find (params[:id])
	end
	def create
		@cashier = current_admin.cashiers.new(param_drop)
		@cashier.adder = current_admin.email
		@cashier.admin = current_admin
		@cashier.active = true
		@cashier.password = "default"
		var = @cashier.name
		@cashier.email = var +"@cashier.com"
		@cashier.save
		redirect_to @cashier
	end
	def edit
		@cashier = Cashier.find(params[:id])
	end
	def update
		@cashier = Cashier.find(params[:id])
		if @cashier.update(param_drop)
			@cashier.encrypted_password = param_drop[:password]
			redirect_to @cashier
		else
			redirect_to cashiers_path
		end
	end
	def activate
		@cashier = Cashier.find(params[:id])
		@cashier.activate
		redirect_to cashiers_path
	end
	def deactivate
		@cashier = Cashier.find(params[:id])
		@cashier.deactivate
		redirect_to cashiers_path
	end
	def index
		@cashiers = Cashier.all
	end
	private
		def param_drop
			params.require(:cashier).permit(:name,:active,:email,:password)
		end
end
