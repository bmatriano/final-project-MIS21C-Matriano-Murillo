class ProductsController < ApplicationController
	before_action :authenticate_admin!
	def index
		@products = Product.all
	end
	def new
	end
	def show
		@product = Product.find(params[:id])
	end
	def create
		@product = Product.new(param_drop)
		@product.status = 'true'
		if @product.save
			redirect_to products_path
		else 
			render 'new'
		end
	end
	def activate
		@product = Product.find(params[:id])
		@product.activate
		redirect_to products_path
	end
	def deactivate
		@product = Product.find(params[:id])
		@product.deactivate
		redirect_to products_path
	end
	def edit
		@product = Product.find(params[:id])
	end
	def update
		@product = Product.find(params[:id])
		if @product.update(param_drop)
			redirect_to @product
		else
			redirect_to products_path
		end
	end
	private
		def param_drop
			params.require(:product).permit(:name,:price,:status, :discount, :description)
		end
end
