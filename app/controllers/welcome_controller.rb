class WelcomeController < ApplicationController
  def home
  end
  def checkpoint
  end
  def verify
  	@display = true 
  	if test_dump[:name] == nil
  		@display = true
  		render 'cashiers/sessions/new'
  	else
				@check = Cashier.find_by_name(test_dump[:name])
				if @check == nil
					render 'home'
				else
					if @check.active == true
						@item = @check.email
						@display = false
						render 'cashiers/sessions/new'
					else
						render 'home'
					end
				end
			end
  #When you wake up, remember to make this controller check if name exists, if user is active, and if both are met, pass values
  #to the sessions controller to ensure that the email is passed bc devise sucks at username auth
  end
  private
	def test_dump
			params.require(:query).permit(:name)
	end
end
