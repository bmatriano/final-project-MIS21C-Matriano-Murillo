class OrdersController < ApplicationController
	def new
		@items = Product.where(status: true)
		@products = Product.where(status: true).pluck(:name)
		@order = Order.new
		@order.orderlines.build
	end
	def index
		@all_orders = Order.all
		@all_lines = Orderline.all
		@all_product = Product.all
	end
	def create
		@pass = Order.new
		@items = Product.all
		@pass.cash = order_params[:cash]
		@pass.total_price = order_params[:i]
		@pass.cashier_id = current_cashier.id 
		@orders = eval(order_params[:hash_vals])
		if @pass.save
			@orders.each do |key,value|
				@new_line = @pass.orderlines.new
				@new_line.order = @pass
				@new_line.order_id = @pass.id
				@new_line.product = @items[@orders[key.to_s]["product"].to_i]
				@new_line.product_id = @orders[key.to_s]["product"].to_i
				@new_line.quantity = @orders[key.to_s]["quantity"].to_i
				if @new_line.product.discount != nil
					@new_line.price = @new_line.product.price * @new_line.quantity * (1 - @new_line.product.discount)
				else 
					@new_line.price = @new_line.product.price * @new_line.quantity 
				end
				@new_line.save
				# {"0"=>{"product"=>"1", "quantity"=>"4", "_destroy"=>"false"}, "1499884172843"=>{"product"=>"3", "quantity"=>"3", "_destroy"=>"false"}}
			end
			# @new_line = @pass.orderlines.new
			# @new_line.order = @pass
			# @new_line.order_id = @pass.id
			# @new_line.product = @items[@orders["0"][:product].to_i-1]
			# @new_line.product_id = @items[@orders["0"][:product].to_i-1]
			# @new_line.quantity = @orders["0"][:quantity].to_i
			# @new_line.save
		else
			redirect_to new
		end
		redirect_to order_path(@pass)
		#render plain: @a
	end
	def show
		@orders = Order.find(params[:id])
		@items = Product.all
		@all_lines= Orderline.all
		@i = @orders.total_price
		@cash = @orders.cash
	end
	def summary
		@items = Product.all
		@orders = order_params[:orderlines_attributes].to_h
		render 'summary'
	end
	def report
		@all_lines = Orderline.where(["created_at >= ?", Time.zone.now.beginning_of_day])
		@all_product = Product.all
	end
	def cashierreport
		@Cashier = Cashier.find(params[:id])
		@all = Orderline.all
		#@lines = @Cashier.orderswhere("created_at >= ? AND cashier_id = ?", Time.zone.now.beginning_of_day, @Cashier.id)
		@all_orders = Order.all.where(["created_at >= ? and cashier_id = ?", Time.zone.now.beginning_of_day, @Cashier.id])
		@all_lines = Array.new()
		@all_orders.each do |order|
		 	@all.each do |line|
		 		if line.order_id == order.id
		 			@all_lines.push(line)
		 		end
		 	end
		end
		@all_product = Product.all
	end
	private
		def order_params
			params.require(:order).permit(:cash , :i, :hash_vals, orderlines_attributes: [:product,:quantity,:done,:_destroy])
		end
		
end
