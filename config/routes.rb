Rails.application.routes.draw do
  devise_scope :admin do
	authenticated :admin do
		root 'cashiers#index'
		resources :cashiers, :products
		put 'deactivate/:id(:format)', :to => 'cashiers#deactivate', :as => :deactivate_cashier
		put 'activate/:id(:format)', :to => 'cashiers#activate', :as => :activate_cashier
		put 'deactivate_product/:id(:format)', :to => 'products#deactivate', :as => :deactivate_product
		put 'activate_product/:id(:format)', :to => 'products#activate', :as => :activate_product
	end
  end
  devise_scope :cashier do
	authenticated :admin do
		root 'orders#new'
	end
  end
  devise_for :cashiers, controllers: {
    sessions:           "cashiers/sessions",
    passwords:          "cashiers/passwords",
    confirmations:      "cashiers/confirmations",
  }
  resources :orders
  devise_for :admins
  root 'welcome#home'
  get 'checkpoint/', :to => 'welcome#checkpoint', :as => :checkpoint
  post 'checkpoint/', :to => 'welcome#verify', :as => :verify
  get 'dailyreport', :to => 'orders#report', :as => :report
  get 'cashierreport/:id', :to => 'orders#cashierreport', :as => :cashierreport
  post 'orders/summary', :to => 'orders#summary', :as => :orders_summary
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
